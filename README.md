# Containers

Respository containing Containerfiles used to manage my container registry

```bash
podman run --rm -it $CONTAINER
```

## Building/Pushing

```bash
# OS containers
podman build --tag localhost/ubuntu:22.04 -f os/ubuntu/Containerfile-22.04
podman push localhost/ubuntu:22.04 registry.gitlab.com/andygeorge/containers/ubuntu:22.04

podman build --tag localhost/debian:10 -f os/debian/Containerfile-10
podman push localhost/debian:10 registry.gitlab.com/andygeorge/containers/debian:10

podman build --tag localhost/debian:11 -f os/debian/Containerfile-11
podman push localhost/debian:11 registry.gitlab.com/andygeorge/containers/debian:11

podman build --tag localhost/fedora:36 -f os/fedora/Containerfile-36
podman push localhost/fedora:36 registry.gitlab.com/andygeorge/containers/fedora:36

podman build --tag localhost/fedora:37 -f os/fedora/Containerfile-37
podman push localhost/fedora:37 registry.gitlab.com/andygeorge/containers/fedora:37

# App containers
podman build --tag localhost/python:3-ubuntu -f app/python/Containerfile-ubuntu
podman push localhost/python:3-ubuntu registry.gitlab.com/andygeorge/containers/python:3-ubuntu

podman build --tag localhost/python:3-debian -f app/python/Containerfile-debian
podman push localhost/python:3-debian registry.gitlab.com/andygeorge/containers/python:3-debian

podman build --tag localhost/python:3-fedora -f app/python/Containerfile-fedora
podman push localhost/python:3-fedora registry.gitlab.com/andygeorge/containers/python:3-fedora

podman build --tag localhost/ansible:2.13.2-ubuntu -f app/ansible/Containerfile-ubuntu
podman push localhost/ansible:2.13.2-ubuntu registry.gitlab.com/andygeorge/containers/ansible:2.13.2-ubuntu

podman build --tag localhost/ansible:2.13.2-fedora -f app/ansible/Containerfile-fedora
podman push localhost/ansible:2.13.2-fedora registry.gitlab.com/andygeorge/containers/ansible:2.13.2-fedora

podman build --tag localhost/check-mk-raw:2.1.0p9 -f app/checkmk/Containerfile
podman push localhost/check-mk-raw:2.1.0p9 registry.gitlab.com/andygeorge/containers/check-mk-raw:2.1.0p9

podman build --tag localhost/pulumi-python:3.37.1-aws -f app/pulumi/Containerfile
podman push localhost/pulumi-python:3.37.1-aws registry.gitlab.com/andygeorge/containers/pulumi-python:3.37.1-aws
```
